(use-modules (gnu)
	     (srfi srfi-1)
	     (gnu services desktop))

(use-package-modules vim
		     wm
		     video
		     certs
		     version-control
		     terminals
		     disk
		     xdisorg
		     web-browsers)

(operating-system
 (host-name "guix-sway")
 (timezone "America/Los_Angeles")
 (locale "en_US.utf8")
 (bootloader (bootloader-configuration
	      (bootloader grub-bootloader)
	      (target "/dev/sda")
	      (timeout 3)))
 (file-systems (cons (file-system
		      (device "/dev/sda1")
		      (mount-point "/")
		      (type "ext4"))
		     %base-file-systems))
 (users (cons (user-account
	       (name "")
	       (group "users")
	       (supplementary-groups '("wheel"
				       "audio"
				       "video"
				       "cdrom")))
	      %base-user-accounts))
 (packages (append (list sway
			 swaybg
			 swayidle
			 swaylock
			 bemenu
			 vim
			 ranger
			 termite
			 luakit
			 youtube-dl
			 mpv
			 nss-certs
			 git)
		   %base-packages))
 (services (remove (lambda (service)
		   (eq? (service-kind service) gdm-service-type))
			 %desktop-services))
 (name-service-switch %mdns-host-lookup-nss))
